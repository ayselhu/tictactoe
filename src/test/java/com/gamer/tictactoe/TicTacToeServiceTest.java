package com.gamer.tictactoe;

import com.gamer.tictactoe.entity.GameStep;
import com.gamer.tictactoe.repository.GameStepRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TicTacToeServiceTest {
    @Captor
    private ArgumentCaptor<GameStep> argumentCaptor;
    private TicTacToeService ticTacToeService;
    @Mock
    private GameStepRepository gameStepRepository;

    @BeforeEach
    public void initial() {
        ticTacToeService = new TicTacToeService(gameStepRepository);
    }

    @Test
    public void whenSaveThenPathAnyArgument() {
        ticTacToeService.play(1, 1);
        verify(gameStepRepository, times(1)).save(any());
    }

    @Test
    public void whenSaveThenPathRealArgument() {
        ticTacToeService.play(1, 1);
        ticTacToeService.play(1, 2);

        var gameStepX = new GameStep(1l, 'X', 1, 1);
        var gameStepO = new GameStep(1l, 'O', 1, 2);

        verify(gameStepRepository, times(1)).save(gameStepX);
        verify(gameStepRepository, times(1)).save(gameStepO);
    }

    @Test
    public void getAllSteps() {
        when(gameStepRepository.findAll()).thenReturn(Arrays.asList(new GameStep(1l, 'X', 1, 0)));
        var gameSteps = ticTacToeService.getGameSteps();
        assertThat(gameSteps).isEqualTo(Arrays.asList(new GameStep(1l, 'X', 2, 1)));
    }

    @Test
    public void getOneStepById() {
        when(gameStepRepository.findById(1l)).thenReturn(Optional.of(new GameStep(1l, 'O', 2, 2)));
        var gameStep = ticTacToeService.getGameStep();
        assertThat(gameStep).isEqualTo(new GameStep(1l, 'O', 2, 2));
    }

    @Test
    public void getArgumentByArgumentCaptor() {
        ticTacToeService.play(1, 1);
        verify(gameStepRepository, times(1)).save(argumentCaptor.capture());
        System.out.println(argumentCaptor.getAllValues());
    }

    @Test
    void whenXOutsideTheBoardThenException() {
        int x = 5;
        int y = 1;
        assertThatThrownBy(() -> ticTacToeService.play(x, y))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("X is outside board");
    }

    @Test
    void whenYOutsideTheBoardThenException() {
        int x = 2;
        int y = 5;
        assertThatThrownBy(() -> ticTacToeService.play(x, y))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Y is outside board");
    }

    @Test
    void whenOccupiedThenRuntimeException() {
        int x = 2;
        int y = 2;
        ticTacToeService.play(x, y);
        assertThatThrownBy(() -> ticTacToeService.play(x, y))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Board is occupied");
    }

    @Test
    public void givenFirstTurnWhenNextPlayerThenX() {
        assertThat(ticTacToeService.nextPlayer()).isEqualTo('X');
    }

    @Test
    public void givenLastTurnWasXWhenNextPlayerThenO() {
        ticTacToeService.play(1, 1);
        assertThat(ticTacToeService.nextPlayer()).isEqualTo('O');
    }

    @Test
    public void whenPlayThenNoWinner() {
        assertThat(ticTacToeService.play(1, 1)).isEqualTo("No Winner");
    }

    @Test
    public void whenPlayAndWholeVerticalLineThenWinnerO() {
        ticTacToeService.play(1, 1); // X
        ticTacToeService.play(1, 2); // O
        ticTacToeService.play(2, 1); // X
        ticTacToeService.play(2, 2); // O
        String actual = ticTacToeService.play(3, 1); // X
        assertThat(actual).isEqualTo("X is the winner");
    }

    @Test
    public void whenPlayAndWholeHorizontalLineThenWinnerO() {
        ticTacToeService.play(2, 1); // X
        ticTacToeService.play(1, 1); // O
        ticTacToeService.play(3, 1); // X
        ticTacToeService.play(1, 2); // O
        ticTacToeService.play(2, 2); // X
        String actual = ticTacToeService.play(1, 3); // O
        assertThat(actual).isEqualTo("O is the winner");
    }

    @Test
    public void whenPlayAndTopBottomDiagonalLineThenWinnerX() {
        ticTacToeService.play(1, 1); // X
        ticTacToeService.play(1, 2); // O
        ticTacToeService.play(2, 2); // X
        ticTacToeService.play(1, 3); // O
        String actual = ticTacToeService.play(3, 3); // X
        assertThat(actual).isEqualTo("X is the winner");
    }

    @Test
    public void whenPlayAndBottomTopDiagonalLineThenWinnerO() {
        ticTacToeService.play(1, 1); // X
        ticTacToeService.play(1, 3); // O
        ticTacToeService.play(1, 2); // X
        ticTacToeService.play(2, 2); // O
        ticTacToeService.play(3, 3); // O
        var actual = ticTacToeService.play(3, 1); // O
        assertThat(actual).isEqualTo("O is the winner");
    }

    @Test
    public void whenAllBoxesAreFilledThenDraw() {
        ticTacToeService.play(1, 1);
        ticTacToeService.play(1, 2);
        ticTacToeService.play(1, 3);
        ticTacToeService.play(2, 1);
        ticTacToeService.play(2, 3);
        ticTacToeService.play(2, 2);
        ticTacToeService.play(3, 1);
        ticTacToeService.play(3, 3);
        var actual = ticTacToeService.play(3, 2);
        assertThat(actual).isEqualTo("The result is draw");
    }
}


