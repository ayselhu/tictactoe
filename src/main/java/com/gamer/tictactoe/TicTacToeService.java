package com.gamer.tictactoe;

import com.gamer.tictactoe.entity.GameStep;
import com.gamer.tictactoe.repository.GameStepRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class TicTacToeService {
    private final GameStepRepository gameStepRepository;

    private final int SIZE = 3;
    private char lastPlayer = '-';
    private char[][] board = {
            {'-', '-', '-'},
            {'-', '-', '-'},
            {'-', '-', '-'}};

    public String play(int x, int y) {
        checkXAxis(x);
        checkYAxis(y);
        lastPlayer = nextPlayer();
        setBox(lastPlayer, x, y);
        save(lastPlayer, x, y);
        if (isWin(x, y))
            return lastPlayer + " is the winner";
        else if (isDraw()) {
            return "The result is draw";
        }
        return "No Winner";
    }

    public List<GameStep> getGameSteps() {
        return gameStepRepository.findAll()
                .stream().map(step -> {
                    step.setX(step.getX() + 1);
                    step.setY(step.getY() + 1);
                    return step;
                }).collect(Collectors.toList());

    }

    public GameStep getGameStep(){
       return gameStepRepository.findById(1l).get();
    }
    private void save(char player, int x, int y) {
        var gameStep = new GameStep(1l, player, x, y);
        gameStepRepository.save(gameStep);
    }

    public char nextPlayer() {
        if (lastPlayer == 'X')
            return 'O';
        else return 'X';
    }

    private void checkXAxis(int x) {
        if (x < 1 || x > 3)
            throw new RuntimeException("X is outside board");
    }

    private void checkYAxis(int y) {
        if (y < 1 || y > 3)
            throw new RuntimeException("Y is outside board");
    }

    private void setBox(char lastPlayer, int x, int y) {
        if (board[x - 1][y - 1] != '-')
            throw new RuntimeException("Board is occupied");
        board[x - 1][y - 1] = lastPlayer;
        printBoard(board);
    }

    private boolean isWin(int x, int y) {
        char diagonal1 = '\0';
        char diagonal2 = '\0';
        char horizontal = '\0';
        char vertical = '\0';
        int playerTotal = lastPlayer * SIZE;
        for (int i = 0; i < SIZE; i++) {
            diagonal1 += board[i][i];
            diagonal2 += board[i][SIZE - i - 1];
            horizontal += board[i][y - 1];
            vertical += board[x - 1][i];
        }
        if (diagonal1 == playerTotal ||
                diagonal2 == playerTotal ||
                horizontal == playerTotal ||
                vertical == playerTotal)
            return true;
        return false;
    }

    private boolean isDraw() {
        for (int x = 0; x < SIZE; x++) {
            for (int y = 0; y < SIZE; y++) {
                if (board[x][y] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    private void printBoard(char[][] board) {
        for (char[] i : board) {
            for (char j : i) {
                System.out.print(j + " ");
            }
            System.out.println();
        }
    }


}
