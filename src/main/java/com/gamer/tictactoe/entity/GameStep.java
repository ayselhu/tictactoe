package com.gamer.tictactoe.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GameStep {
    @Id
    @GeneratedValue
    private Long id;
    private char player;
    private int x;
    private int y;

}
