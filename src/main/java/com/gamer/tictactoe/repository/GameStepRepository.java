package com.gamer.tictactoe.repository;

import com.gamer.tictactoe.entity.GameStep;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GameStepRepository extends JpaRepository<GameStep, Long> {
}
